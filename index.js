// Require objects.
var express  = require('express');
var app      = express();
var aws      = require('aws-sdk');
var queueUrl = "https://sqs.eu-central-1.amazonaws.com/574220522774/AmazonPriceChangeNotifications";
var kafkaClient = require('./kafka-client');

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

// // Creating a queue.
// app.get('/create', function (req, res) {
//     var params = {
//         QueueName: "MyFirstQueue"
//     };
//
//     sqs.createQueue(params, function(err, data) {
//         if(err) {
//             res.send(err);
//         }
//         else {
//             res.send(data);
//         }
//     });
// });
//
// // Listing our queues.
// app.get('/list', function (req, res) {
//     sqs.listQueues(function(err, data) {
//         if(err) {
//             res.send(err);
//         }
//         else {
//             res.send(data);
//         }
//     });
// });

// Sending a message.
// NOTE: Here we need to populate the queue url you want to send to.
// That variable is indicated at the top of app.js.
app.get('/send', function (req, res) {
    let numOfMessagesToSend = req.query.total ? req.query.total:1;
    let messagesSent = [];
    for(let i =0 ;i < numOfMessagesToSend; i++){
        let nowMili = Date.now();
        var params = {
            MessageBody: `<message><hello>Hello World</hello><sent_time_stamp>${nowMili}</sent_time_stamp></message>`,
            QueueUrl: queueUrl,
            DelaySeconds: 0
        };
        sqs.sendMessage(params, function (err, data) {
            if (err) {
                console.log(err);
            }
            else {
                console.log(data);
            }
        });
    }
    res.send('sent');
});

// Start server.
var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('AWS SQS example app listening at http://%s:%s', host, port);
});



