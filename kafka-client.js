
var kafka    = require('kafka-node');
var promClient = require('prom-client');
var Consumer = kafka.Consumer;
var Client = kafka.Client;
var topic = 'Notifications';

const gauge = new promClient.Gauge({ name: 'avg_handle_time', help: 'metric_help', labelNames: [ 'sqs_kafka', 'avg_time' ] });


var client = new Client('hkt-kafka.feedvisor.com:9092');
var topics = [
    {topic: topic, partition: 0}
];
var options = { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 };
var consumer = new Consumer(client, topics, options);

consumer.on('message', function (message) {
    console.log(message);
    if(message.message){
        let currTime = Date.now();
        let totalTimeMili = currTime - message.sent_time_stamp;
        gauge.set(totalTimeMili, new Date());
    }
});

consumer.on('error', function (err) {
    console.log('error', err);
});

console.log(`Listening to kafka at ${client.connectionString}`);

